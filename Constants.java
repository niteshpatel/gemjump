package com.herodevelop.gemjump;

import com.herodevelop.hdfw.utils.Color;

public class Constants {
    public static final Color COLOR_BG = new Color(0.1484375f, 0.1484375f, 0.1484375f);

    public static enum BonusType {
        BOMB, TIMER
    }
}
