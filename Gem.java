package com.herodevelop.gemjump;

import com.herodevelop.gemjump.screens.ScreenGame;
import com.herodevelop.hdfw.Drawable;
import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdlibgdx.Graphics.Image;

public final class Gem implements Drawable {

    public final int gemId;
    private final ScreenEx screen;
    private int posX;
    private int posY;
    private int gemX;
    private int gemY;
    private Image image;
    private boolean blockedState;

    public Gem(ScreenGame screen, int gemId, int posX, int posY) {
        this.screen = screen;
        this.gemId = gemId;
        this.gemX = posX;
        this.gemY = posY;

        // Set the gem position
        this.setPosX((posX * 100) + 10);
        this.setPosY((posY * 100) + 220);

        // Set the gem image
        this.image = screen.getGemImagesNormal().get(gemId);
        this.setBlockedState(false);
    }

    public final void paint() {
        screen.game.graphics.drawImage(image, this.getPosX(), this.getPosY());
    }

    public final Image getImage() {
        return image;
    }

    public final void setImage(Image image) {
        this.image = image;
    }

    public final int getGemX() {
        return gemX;
    }

    public final void setGemX(int gemX) {
        this.gemX = gemX;
    }

    public final int getGemY() {
        return gemY;
    }

    public final void setGemY(int gemY) {
        this.gemY = gemY;
    }

    public final int getPosX() {
        return posX;
    }

    public final void setPosX(int posX) {
        this.posX = posX;
    }

    public final int getPosY() {
        return posY;
    }

    public final void setPosY(int posY) {
        this.posY = posY;
    }

    public final int getWidth() {
        return 100;
    }

    public final int getHeight() {
        return 100;
    }

    public boolean isBlockedState() {
        return blockedState;
    }

    public void setBlockedState(boolean blockedState) {
        this.blockedState = blockedState;
    }
}