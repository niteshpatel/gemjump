package com.herodevelop.gemjump.screens;

import com.herodevelop.gemjump.Constants.BonusType;
import com.herodevelop.gemjump.MainStarter.Screens;
import com.herodevelop.hdfw.GameEx;
import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdlibgdx.Graphics.Image;

public final class ScreenSplash extends ScreenEx {

    private final Image bg;
    private Boolean billingLoaded;
    private Boolean screensLoaded;
    private Boolean painted;
    private Float timer;

    public ScreenSplash(GameEx game) {
        super(game);
        this.bg = new Image("backgrounds/splash.png");
        this.billingLoaded = false;
        this.screensLoaded = false;
        this.painted = false;
        this.timer = null;
    }

    @Override
    public final void onStart() {
        timer = 3f;

        // Start timer
        addTask(new ShowGameTask(this));

        // Main screen task
        addTask(new BgTask(this));

        // Load remaining screens
        addTask(new CreateExTask(this));

        // Setup billing task
        addTask(new SetupBillingTask(this));

        // Load user settings
        String scoreKey = "score";
        String highscoresKey = "highscores";
        String levelKey = "level";
        String progressKey = "progress";
        String livesKey = "lives";
        String coinsKey = "coins";
        // Keys for bonuses
        String bombBonusKey = BonusType.BOMB.name();
        String timerBonusKey = BonusType.TIMER.name();

        // Store user settings in the game data
        game.data.put(scoreKey, game.app.getPreference(scoreKey, "0"));
        game.data.put(highscoresKey, game.app.getPreference(highscoresKey, "[0, 0, 0, 0, 0]"));
        game.data.put(levelKey, game.app.getPreference(levelKey, "0"));
        game.data.put(progressKey, game.app.getPreference(progressKey, "0"));
        game.data.put(livesKey, game.app.getPreference(livesKey, "0"));
        game.data.put(coinsKey, game.app.getPreference(coinsKey, "0"));
        game.data.put(bombBonusKey, game.app.getPreference(bombBonusKey, "0"));
        game.data.put(timerBonusKey, game.app.getPreference(timerBonusKey, "0"));
    }

    private final class ShowGameTask extends Task {

        public ShowGameTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void update(float delta) {
            timer -= delta;
            if (timer <= 0 && screensLoaded && billingLoaded) {
                game.setScreenEx(Screens.MENU);
            }
        }
    }

    private final class BgTask extends Task {

        public BgTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void paint(float delta) {
            game.graphics.drawImage(bg, 0, 0);
            painted = true;
        }
    }

    private final class CreateExTask extends Task {

        public CreateExTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void update(float delta) {
            // Only start this once the screen has been painted once
            if (painted && !screensLoaded) {
                game.createEx();
                screensLoaded = true;
            }
        }

        @Override
        public final boolean isDone() {
            return screensLoaded;
        }
    }

    private final class SetupBillingTask extends Task {
        int state = 0;

        public SetupBillingTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void update(float delta) {

            // Setup billing.  We manage the async tasks by using a state variable, and check the current
            // state every time we hit update.
            if (state == 0) {
                game.billing.startSetup();
                state = 1;
            }
            if (state == 1) {
                if (game.billing.isIabHelperSetup()) {
                    state = 2;
                    game.billing.queryInventoryAsync();
                }
            }
            if (state == 2) {
                if (!game.billing.isAsyncInProgress()) {
                    state = 3;
                    game.billing.consumeAsync();
                }
            }
            if (state == 3) {
                if (!game.billing.isAsyncInProgress()) {
                    state = 4;
                    billingLoaded = true;
                }
            }
        }

        @Override
        public final boolean isDone() {
            return billingLoaded;
        }
    }
}
