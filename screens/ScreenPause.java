package com.herodevelop.gemjump.screens;

import com.herodevelop.gemjump.MainStarter.Screens;
import com.herodevelop.hdfw.GameEx;
import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdfw.ScreenId;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdfw.tasks.actions.ButtonTask;
import com.herodevelop.hdfw.tasks.actions.SetScreenTask;
import com.herodevelop.hdfw.tasks.transitions.FadeInTask;
import com.herodevelop.hdfw.tasks.transitions.FadeOutTaskFactory;
import com.herodevelop.hdlibgdx.Graphics.Image;

public final class ScreenPause extends ScreenEx {

    private final Image bg;
    private final Image continueButton;
    private final Image restartButton;
    private final Image quitButton;

    public ScreenPause(GameEx game) {
        super(game);
        this.bg = new Image("backgrounds/pause.png");
        this.continueButton = new Image("artefacts/button_continue.png");
        this.restartButton = new Image("artefacts/button_restart.png");
        this.quitButton = new Image("artefacts/button_quit.png");
    }

    @Override
    protected final void onStart() {
        // Main screen task
        addTask(new BgTask(this));
        addTask(new FadeInTask(this, 400));

        // Add continue, restart, quit button tasks
        addTask(new ButtonTask(this, continueButton, 360, 700,
                new FadeOutTaskFactory(this, 400, new SetScreenTask(this, Screens.GAME, 0.1f))));
        addTask(new ButtonTask(this, restartButton, 360, 550,
                new FadeOutTaskFactory(this, 400, new StopGameSetScreenTask(this, Screens.GAME, 0.1f))));
        addTask(new ButtonTask(this, quitButton, 360, 400,
                new FadeOutTaskFactory(this, 400, new StopGameSetScreenTask(this, Screens.MENU, 0.1f))));
    }

    // This is a very dodgy way to nail a bit of functionality on the
    // set screen task by overriding and messing with the update function.
    // This kind of use-case needs to be thought about
    private final class StopGameSetScreenTask extends SetScreenTask {
        boolean stopDone;

        public StopGameSetScreenTask(ScreenEx screen, ScreenId screenId, Float delay) {
            super(screen, screenId, delay);
            stopDone = false;
        }

        @Override
        public void update(float delta) {
            super.update(delta);
            if (!stopDone) {
                screen.game.getScreenEx(Screens.GAME).stop();
                stopDone = true;
            }
        }
    }

    private final class BgTask extends Task {

        public BgTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void paint(float delta) {
            game.graphics.drawImage(bg, 0, 0);
        }
    }
}
