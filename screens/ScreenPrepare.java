package com.herodevelop.gemjump.screens;

import com.herodevelop.gemjump.Constants;
import com.herodevelop.gemjump.Constants.BonusType;
import com.herodevelop.gemjump.MainStarter.Screens;
import com.herodevelop.gemjump.tasks.artefacts.ProgressTask;
import com.herodevelop.gemjump.tasks.bonuses.BuyBonusTaskFactory;
import com.herodevelop.hdfw.GameEx;
import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdfw.tasks.actions.ButtonTask;
import com.herodevelop.hdfw.tasks.actions.SetScreenTask;
import com.herodevelop.hdfw.tasks.transitions.FadeInTask;
import com.herodevelop.hdfw.tasks.transitions.FadeOutTaskFactory;
import com.herodevelop.hdlibgdx.Graphics.Font;
import com.herodevelop.hdlibgdx.Graphics.Image;

public final class ScreenPrepare extends ScreenEx {

    private final Image bg;
    private final Image playButton;
    private final Image buyButton;
    private final Image bonusBomb;
    private final Image bonusTimer;
    private final Font fontBonus;
    private final Font fontCost;
    public int bonusBombCount;
    public int bonusTimerCount;
    public ProgressTask progressTask;

    public ScreenPrepare(GameEx game) {
        super(game);
        this.bg = new Image("backgrounds/prepare.png");
        this.playButton = new Image("artefacts/button_play.png");
        this.buyButton = new Image("artefacts/button_buy.png");

        // Bonus images
        this.bonusBomb = new Image("bonuses/bomb.png");
        this.bonusTimer = new Image("bonuses/timer.png");

        // Setup fonts
        this.fontBonus = new Font("fonts/terminal-40-black.fnt", false);
        this.fontCost = new Font("fonts/terminal-40.fnt", false);
    }

    @Override
    protected final void onStart() {
        // Main screen task
        addTask(new BgTask(this));
        addTask(new FadeInTask(this, 400));
        addTask(new ButtonTask(this, playButton, 356, 199,
                new FadeOutTaskFactory(this, 400, new SetScreenTask(this, Screens.GAME, 0.1f))));

        // Get bonus inventory
        this.bonusBombCount = Integer.parseInt(game.data.get(BonusType.BOMB.name()));
        this.bonusTimerCount = Integer.parseInt(game.data.get(BonusType.TIMER.name()));

        // Add bonus buy buttons
        addTask(new ButtonTask(this, buyButton, 480, 750,
                new BuyBonusTaskFactory(this, BonusType.BOMB, 100)));
        addTask(new ButtonTask(this, buyButton, 480, 640,
                new BuyBonusTaskFactory(this, BonusType.TIMER, 200)));

        // Progress task
        progressTask = new ProgressTask(this);
        addTask(progressTask);
    }

    public final void updateBonusCount(BonusType bonusType) {
        if (bonusType == BonusType.BOMB)
            bonusBombCount++;
        if (bonusType == BonusType.TIMER)
            bonusTimerCount++;
    }

    private final class BgTask extends Task {

        public BgTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void paint(float delta) {
            game.graphics.drawImage(bg, 0, 0);

            // Draw bomb inventory
            game.graphics.drawRect(0, 700, 380, 100, 1, 1, 1, 1);
            game.graphics.drawFont("Bombs", fontBonus, 15, 774);
            game.graphics.drawRect(200, 710, 80, 80, Constants.COLOR_BG.red, Constants.COLOR_BG.green, Constants.COLOR_BG.blue, 1);
            game.graphics.drawImage(bonusBomb, 191, 701);
            game.graphics.drawRect(290, 710, 80, 80, Constants.COLOR_BG.red, Constants.COLOR_BG.green, Constants.COLOR_BG.blue, 1);
            game.graphics.drawFont(Integer.toString(bonusBombCount), fontCost, 312, 774);

            // Draw bomb buy control
            game.graphics.drawRect(550, 700, 160, 100, 1, 1, 1, 1);
            game.graphics.drawRect(560, 710, 140, 80, Constants.COLOR_BG.red, Constants.COLOR_BG.green, Constants.COLOR_BG.blue, 1);
            game.graphics.drawFont("100c", fontCost, 567, 774);

            // Draw timer inventory
            game.graphics.drawRect(0, 590, 380, 100, 1, 1, 1, 1);
            game.graphics.drawFont("Timer", fontBonus, 15, 664);
            game.graphics.drawRect(200, 600, 80, 80, Constants.COLOR_BG.red, Constants.COLOR_BG.green, Constants.COLOR_BG.blue, 1);
            game.graphics.drawImage(bonusTimer, 190, 588);
            game.graphics.drawRect(290, 600, 80, 80, Constants.COLOR_BG.red, Constants.COLOR_BG.green, Constants.COLOR_BG.blue, 1);
            game.graphics.drawFont(Integer.toString(bonusTimerCount), fontCost, 312, 664);

            // Draw timer buy control
            game.graphics.drawRect(550, 590, 160, 100, 1, 1, 1, 1);
            game.graphics.drawRect(560, 600, 140, 80, Constants.COLOR_BG.red, Constants.COLOR_BG.green, Constants.COLOR_BG.blue, 1);
            game.graphics.drawFont("200c", fontCost, 567, 664);
        }
    }
}
