package com.herodevelop.gemjump.screens;

import com.herodevelop.gemjump.Constants.BonusType;
import com.herodevelop.gemjump.Gem;
import com.herodevelop.gemjump.MainStarter.Screens;
import com.herodevelop.gemjump.tasks.bonuses.BombBonusTask;
import com.herodevelop.gemjump.tasks.bonuses.TimerBonusTask;
import com.herodevelop.gemjump.tasks.effects.BlockedGemTask;
import com.herodevelop.hdfw.GameEx;
import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdfw.ScreenId;
import com.herodevelop.hdfw.helpers.Match3;
import com.herodevelop.hdfw.helpers.ShadowDrawable;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdfw.tasks.actions.ButtonTask;
import com.herodevelop.hdfw.tasks.actions.SetScreenTask;
import com.herodevelop.hdfw.tasks.effects.FadeExpandTask;
import com.herodevelop.hdfw.tasks.effects.FadeTask;
import com.herodevelop.hdfw.tasks.effects.MoveDownTask;
import com.herodevelop.hdfw.tasks.transitions.FadeOutTaskFactory;
import com.herodevelop.hdlibgdx.Audio.Sound;
import com.herodevelop.hdlibgdx.Graphics.Font;
import com.herodevelop.hdlibgdx.Graphics.Image;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public final class ScreenGame extends ScreenEx {

    public final Match3 match3;
    private final Image bg;
    private final Image pauseButton;
    private final Image timerBar;
    private final Image gemBurstImage;
    private final Image gemHintImage;
    private final Sound[] gemChain;
    private final HashMap<Integer, Image> gemImagesNormal;
    private final HashMap<Integer, Image> gemImagesBlocked;
    private final Font fontScore;
    private final Font fontTimer;
    private final NumberFormat formatter;
    public Gem[][] gems;
    public Float timer;
    private Integer score;

    public ScreenGame(GameEx game) {
        super(game);

        // Background pre-load
        this.bg = new Image("backgrounds/bg_game.png");
        this.pauseButton = new Image("artefacts/button_pause.png");
        this.timerBar = new Image("artefacts/timerbar.png");

        // Gems normal pre-load
        this.gemImagesNormal = new HashMap<Integer, Image>();
        this.getGemImagesNormal().put(0, new Image("gems/gem_blue.png"));
        this.getGemImagesNormal().put(1, new Image("gems/gem_green.png"));
        this.getGemImagesNormal().put(2, new Image("gems/gem_purple.png"));
        this.getGemImagesNormal().put(3, new Image("gems/gem_red.png"));
        this.getGemImagesNormal().put(4, new Image("gems/gem_yellow.png"));

        // Gems blocked pre-load
        this.gemImagesBlocked = new HashMap<Integer, Image>();
        this.getGemImagesBlocked().put(0, new Image("gems/gem_blue_blocked.png"));
        this.getGemImagesBlocked().put(1, new Image("gems/gem_green_blocked.png"));
        this.getGemImagesBlocked().put(2, new Image("gems/gem_purple_blocked.png"));
        this.getGemImagesBlocked().put(3, new Image("gems/gem_red_blocked.png"));
        this.getGemImagesBlocked().put(4, new Image("gems/gem_yellow_blocked.png"));

        // Gems chain sounds pre-load
        this.gemChain = new Sound[6];
        this.gemChain[0] = new Sound("sounds/gem_chain1.ogg");
        this.gemChain[1] = new Sound("sounds/gem_chain2.ogg");
        this.gemChain[2] = new Sound("sounds/gem_chain3.ogg");
        this.gemChain[3] = new Sound("sounds/gem_chain4.ogg");
        this.gemChain[4] = new Sound("sounds/gem_chain5.ogg");
        this.gemChain[5] = new Sound("sounds/gem_chain6.ogg");

        // Remove and hint animation pre-load
        this.gemHintImage = new Image("effects/square.png");
        this.gemBurstImage = new Image("effects/square.png");

        // Array to store gems
        this.gems = null;

        // Score and timer
        this.score = null;
        this.timer = null;

        // Get a match-3 helper and handler
        this.match3 = new Match3();
        this.match3.setMatch3Handler(new Match3Handler(this));

        // Setup font
        this.fontScore = new Font("fonts/terminal-40.fnt", false);
        this.fontTimer = new Font("fonts/terminal-40.fnt", false);

        // Number formatter
        this.formatter = NumberFormat.getInstance(new Locale("en_US"));
    }

    @Override
    protected final void onStart() {
        // Initialise gems
        int offset;
        gems = new Gem[7][8];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 8; j++) {
                int gemId = (int) Math.floor(Math.random() * 5);

                // The delay is exponentially proportional to the
                // distance the gems are from the origin - this gives
                // the desired animation effect
                offset = (int) (Math.pow((double) i + (j * 1.3), 1.7));
                gems[i][j] = new Gem(this, gemId, i, j + offset);
                addTask(new MoveDownTask(this, gems[i][j], offset * 100, 100));

                // Set the initial position of the gem
                gems[i][j].setGemX(i);
                gems[i][j].setGemY(j);
            }
        }

        // Set up timer and score
        score = 0;
        timer = 60f;

        // Main screen task
        addTask(new BgTask(this));

        // Gems task
        addTask(new GemsTask(this));

        // Pause button
        addTask(new ButtonTask(this, pauseButton, 660, 1175,
                new FadeOutTaskFactory(this, 400, new SuspendGameSetScreenTask(this, Screens.PAUSE, 0.1f))));

        // Score task
        addTask(new ScoreTask(this));

        // Timer task
        addTask(new TimerTask(this));
    }

    public HashMap<Integer, Image> getGemImagesNormal() {
        return gemImagesNormal;
    }

    public HashMap<Integer, Image> getGemImagesBlocked() {
        return gemImagesBlocked;
    }

    // This is a very dodgy way to nail a bit of functionality on the
    // set screen task by overriding and messing with the update function.
    // This kind of use-case needs to be thought about
    private final class SuspendGameSetScreenTask extends SetScreenTask {
        boolean suspendDone;

        public SuspendGameSetScreenTask(ScreenEx screen, ScreenId screenId, Float delay) {
            super(screen, screenId, delay);
            reset();
        }

        @Override
        public void reset() {
            super.reset();
            suspendDone = false;
        }

        @Override
        public void update(float delta) {
            super.update(delta);
            if (!suspendDone) {
                suspend();
                suspendDone = true;
            }
        }
    }

    // Create a custom handler for Match3 handling
    private final class Match3Handler implements Match3.Match3Handler {

        private final ScreenEx screen;

        public Match3Handler(ScreenEx screen) {
            this.screen = screen;
        }

        @Override
        public void postDropDown(Object object, int count) {
            Gem gem = (Gem) object;
            gem.setGemY(gem.getGemY() - count);
            addTask(new MoveDownTask(screen, gem, count * 100, 40));
        }

        @Override
        public void postRemoveChain(Object object) {
            ShadowDrawable gemBurst = new ShadowDrawable((Gem) object, gemBurstImage);
            addTask(new FadeExpandTask(screen, gemBurst, 0.33f, 200));
        }

        @Override
        public int itemKey(Object object) {
            return ((Gem) object).gemId;
        }
    }

    private final class BgTask extends Task {

        public BgTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void paint(float delta) {
            game.graphics.drawImage(bg, 0, 0);
        }
    }

    public final class GemsTask extends Task {
        public BombBonusTask bombBonusTask;
        public TimerBonusTask timerBonusTask;
        private boolean bombBonusEnabled;
        private boolean timerBonusEnabled;
        private float timeSincePlay;
        private float timeSinceBalance;
        private float timeSinceAutoPlay;
        private float timeSinceHint;
        private float timeToNextBonusAreaBomb;
        private float timeToNextBonusTimer;
        private boolean autoPlayCheck;
        private boolean autoPlayOn;
        private boolean inBonusRound;
        private int chainCount;

        public GemsTask(ScreenEx screen) {
            super(screen);

            this.timeSincePlay = 0;
            this.timeSinceBalance = 0;
            this.timeSinceAutoPlay = 0;
            this.timeSinceHint = 0;

            // Initial time to first bonus is random
            this.timeToNextBonusAreaBomb = (float) Math.floor(Math.random() * 3) + 2;
            this.timeToNextBonusTimer = (float) Math.floor(Math.random() * 3) + 2;
            this.autoPlayCheck = false;
            this.autoPlayOn = false;
            this.inBonusRound = false;
            this.chainCount = 0;

            // Current bonus tasks
            this.bombBonusEnabled = false;
            this.bombBonusTask = null;
            this.timerBonusEnabled = false;
            this.timerBonusTask = null;

            // Get current bonus counts
            int bombBonusCount = Integer.parseInt(game.data.get(BonusType.BOMB.name()));
            int timerBonusCount = Integer.parseInt(game.data.get(BonusType.TIMER.name()));

            // Use bonuses if we have any
            if (bombBonusCount > 0) {
                bombBonusCount--;
                bombBonusEnabled = true;
                game.data.put(BonusType.BOMB.name(), String.valueOf(bombBonusCount));
                game.app.setPreference(BonusType.BOMB.name(), String.valueOf(bombBonusCount));
            }
            if (timerBonusCount > 0) {
                timerBonusCount--;
                timerBonusEnabled = true;
                game.data.put(BonusType.TIMER.name(), String.valueOf(timerBonusCount));
                game.app.setPreference(BonusType.TIMER.name(), String.valueOf(timerBonusCount));
            }
            game.app.flushPreferences();
        }

        // Provide a hint if the user has not clicked anything for a while
        private void provideHint(float delta) {
            // If we have played or hinted recently, return
            timeSincePlay += delta;
            timeSinceHint += delta;
            if (timeSincePlay < 3 || timeSinceHint < 0.7) {
                return;
            }

            // Provide a hint
            timeSinceHint = 0;

            // Get the chain count for the current grid state
            Integer[][] checked = new Integer[gems.length][gems[0].length];
            HashMap<Integer, Integer> chainCount = match3.countChains(gems, checked);

            // Mark the biggest chain found
            int count = match3.getCount(checked, chainCount.get(0));
            if (count > 2) {
                int symbol = chainCount.get(0);
                for (int i = 0; i < 7; i++) {
                    for (int j = 0; j < 8; j++) {
                        if (checked[i][j] == symbol) {
                            ShadowDrawable gemHint = new ShadowDrawable(gems[i][j], gemHintImage);
                            addTask(new FadeTask(screen, gemHint, 1f, 200));
                        }
                    }
                }
            }
        }

        // This method auto-plays
        private void autoPlay(float delta) {
            // Check for special sequence
            if (!autoPlayOn && game.input.justTouched()) {
                int posX = game.input.getX();
                int posY = game.input.getY();

                // Calculate gem positions
                int gemX = (int) Math.floor((posX - 5) / 50);
                int gemY = (int) Math.floor((posY - 110) / 50);

                // Check first sequence
                if (!autoPlayCheck) {
                    if (gemX == 1 && gemY == 2) {
                        autoPlayCheck = true;
                    }
                }

                // Check second sequence
                else {
                    if (gemX == 5 && gemY == 5) {
                        autoPlayOn = true;
                    }
                    autoPlayCheck = false;
                }
            }

            // Return if auto-play is not turned on
            if (!autoPlayOn) {
                return;
            }

            // If we have played recently, return
            timeSinceAutoPlay += delta;
            if (timeSinceAutoPlay < 0.5) {
                return;
            }

            // Reset the auto-play time
            timeSinceAutoPlay = 0;

            // Get the chain count for the current grid state
            Integer[][] checked = new Integer[gems.length][gems[0].length];
            HashMap<Integer, Integer> chainCount = match3.countChains(gems, checked);

            // Drop the biggest chain found
            int count = match3.getCount(checked, chainCount.get(0));
            if (count > 2) {
                match3.removeChain(gems, checked, chainCount.get(0));
                match3.dropDown(gems);

                // Post drop tasks
                postDrop(count);
            }
        }

        // This method adds chains to help the user, if there are not enough
        // large (size-3) chains on the grid
        private void balanceChains(float delta) {
            // If we have balanced recently, return
            timeSinceBalance += delta;
            if (timeSinceBalance < 1) {
                return;
            }

            // Reset the balance time
            timeSinceBalance = 0;

            // Get the chain count for the current grid state
            Integer[][] checked = new Integer[gems.length][gems[0].length];
            HashMap<Integer, Integer> chainCount = match3.countChains(gems, checked);
            Integer count1 = chainCount.get(1);
            Integer count2 = chainCount.get(2);
            if (count1 == null) {
                count1 = 0;
            }
            if (count2 == null) {
                count2 = 0;
            }

            // Get the total number of blocks made up of small chains
            // is greater than 47 then we should help the user and create
            // more chains on the grid
            Integer smallTotal = count1 + (count2 * 2);
            if (smallTotal > 44) {
                int gemId = (int) Math.floor(Math.random() * 5);
                int shapeId = (int) Math.floor(Math.random() * 3);

                // The offset is used to position the chains randomly, but
                // we ignore the top half of the grid, since chains in the
                /// bottom half potentially generate more chains on drop
                int offsetX = (int) Math.floor(Math.random() * 5);
                int offsetY = (int) Math.floor(Math.random() * 3);

                // Positions for the new gems
                int gem1posX = 0;
                int gem1posY = 0;
                int gem2posX = 0;
                int gem2posY = 0;
                int gem3posX = 0;
                int gem3posY = 0;

                // Line shape
                if (shapeId == 0) {
                    gem1posX = offsetX;
                    gem1posY = offsetY;
                    gem2posX = offsetX + 1;
                    gem2posY = offsetY;
                    gem3posX = offsetX + 2;
                    gem3posY = offsetY;
                }

                // L shape
                if (shapeId == 1) {
                    offsetX += (int) Math.floor(Math.random() * 1);
                    gem1posX = offsetX;
                    gem1posY = offsetY;
                    gem2posX = offsetX;
                    gem2posY = offsetY + 1;
                    gem3posX = offsetX + 1;
                    gem3posY = offsetY;
                }

                // Reverse L shape
                if (shapeId == 2) {
                    offsetX += (int) Math.floor(Math.random() * 1);
                    gem1posX = offsetX;
                    gem1posY = offsetY;
                    gem2posX = offsetX + 1;
                    gem2posY = offsetY;
                    gem3posX = offsetX + 1;
                    gem3posY = offsetY + 1;
                }

                // Create the gems
                gems[gem1posX][gem1posY] = new Gem((ScreenGame) screen, gemId, gem1posX, gem1posY);
                gems[gem2posX][gem2posY] = new Gem((ScreenGame) screen, gemId, gem2posX, gem2posY);
                gems[gem3posX][gem3posY] = new Gem((ScreenGame) screen, gemId, gem3posX, gem3posY);

                // Check for invalid bonuses
                updateBonuses();
            }
        }

        // Find a gem in a chain
        private Gem getGemInChain(Integer[][] inUse) {
            // Variables to store checked locations and chain counts
            Integer[][] checked = new Integer[gems.length][gems[0].length];
            HashMap<Integer, Integer> chainCount = match3.countChains(gems, checked);

            // Pick a random chain
            int max = match3.getCount(checked, chainCount.get(0));
            if (max > 2) {

                // Get the total number of chains
                int count = 0;
                for (int key : chainCount.keySet()) {
                    if (key != 0)
                        count += chainCount.get(key);
                }

                // Get a list of symbols with chains larger than 2
                List<Integer> symbols = new ArrayList<Integer>();
                int symbol = 1;
                int symbolCount;
                while (symbol < count) {
                    symbol++;
                    symbolCount = match3.getCount(checked, symbol);
                    if (symbolCount > 2)
                        symbols.add(symbol);
                }

                symbol = symbols.get((int) Math.floor(Math.random() * symbols.size()));
                int chosen = (int) Math.floor(Math.random() * match3.getCount(checked, symbol));
                count = 0;
                for (int i = 0; i < 7; i++) {
                    for (int j = 0; j < 8; j++) {
                        if (inUse[i][j] == 0 && checked[i][j] == symbol) {
                            if (count == chosen)
                                return gems[i][j];
                            else
                                count++;
                        }
                    }
                }
            }
            return null;
        }

        // Creates a bonus as necessary
        private void createBonus(float delta) {
            // Get existing positions with bonus
            Integer[][] checked = new Integer[gems.length][gems[0].length];
            for (int i = 0; i < gems.length; i++) {
                for (int j = 0; j < gems[0].length; j++) {
                    checked[i][j] = 0;
                }
            }
            if (bombBonusTask != null) {
                match3.getChain(gems, bombBonusTask.gem.getGemX(), bombBonusTask.gem.getGemY(), checked, 1);
            }
            if (timerBonusTask != null) {
                match3.getChain(gems, timerBonusTask.gem.getGemX(), timerBonusTask.gem.getGemY(), checked, 1);
            }

            // Area bomb
            if (bombBonusEnabled && bombBonusTask == null) {
                timeToNextBonusAreaBomb -= delta;
                if (timeToNextBonusAreaBomb < 0) {
                    Gem gem = getGemInChain(checked);
                    if (gem != null) {
                        timeToNextBonusAreaBomb = (float) Math.floor(Math.random() * 4) + 3;
                        bombBonusTask = new BombBonusTask((ScreenGame) screen, this, gem);
                        addTask(bombBonusTask);
                        return;
                    }
                }
            }

            // Timer
            if (timerBonusEnabled && timerBonusTask == null) {
                timeToNextBonusTimer -= delta;
                if (timeToNextBonusTimer < 0) {
                    Gem gem = getGemInChain(checked);
                    if (gem != null) {
                        timeToNextBonusTimer = (float) Math.floor(Math.random() * 4) + 3;
                        timerBonusTask = new TimerBonusTask((ScreenGame) screen, this, gem);
                        addTask(timerBonusTask);
                        return;
                    }
                }
            }
        }

        private void replaceGems() {
            // Replace missing gems
            int gemId;
            for (int i = 0; i < 7; i++) {
                for (int j = 0; j < 8; j++) {
                    if (gems[i][j] == null) {
                        gemId = (int) Math.floor(Math.random() * 5);
                        gems[i][j] = new Gem((ScreenGame) screen, gemId, i, j + 5);
                        addTask(new MoveDownTask(screen, gems[i][j], 500, 60));

                        // Set the initial position of the gem
                        gems[i][j].setGemX(i);
                        gems[i][j].setGemY(j);
                    }
                }
            }
        }

        @Override
        public final void handleInput(float delta) {
            if (game.input.justTouched()) {
                int posX = game.input.getX();
                int posY = game.input.getY();

                // Check for gem presses
                if (posX >= 5 && posY >= 110 && posX < 355 && posY < 510) {

                    // Calculate gem positions
                    int gemX = (int) Math.floor((posX - 5) / 50);
                    int gemY = (int) Math.floor((posY - 110) / 50);

                    // Check for a chain
                    Integer[][] chain = match3.getChain(gems, gemX, gemY, null, 1);
                    int count = match3.getCount(chain, 1);

                    // Only remove gems if there was chain larger than 2
                    if (count > 2) {

                        // Now remove the games
                        match3.removeChain(gems, chain, 1);
                        match3.dropDown(gems);

                        // Post drop tasks
                        postDrop(count);
                    }

                    // Show blocked gem animation for chain size 1-2
                    else if (count > 0) {
                        for (int i = 0; i < gems.length; i++) {
                            for (int j = 0; j < gems[0].length; j++) {
                                if (chain[i][j] != 0) {
                                    if (!gems[i][j].isBlockedState()) {
                                        addTask(new BlockedGemTask((ScreenGame) screen, gems[i][j]));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Perform post gem drop tasks
        public void postDrop(int count) {

            // Update the score
            score += (360 * count - 300) * (int) Math.pow(1.3f, (float) chainCount);

            // Replace removed gems
            replaceGems();

            // Update the chain count
            if (chainCount < 6)
                chainCount++;

            // Trigger bonus round if chain hits limit
            if (!inBonusRound && chainCount > 5) {
                addTask(new BonusRound(screen, this));
            }

            // Play sound
            ((ScreenGame) screen).gemChain[Math.min(chainCount - 1, 5)].play(0.6f);

            // Update time since play
            timeSincePlay = 0;

            // Reset hint status
            timeSinceHint = 0;

            // Remove invalid bonuses
            updateBonuses();
        }

        private void updateBonuses() {
            if (bombBonusTask != null) {
                Integer[][] checked = match3.getChain(gems, bombBonusTask.gem.getGemX(), bombBonusTask.gem.getGemY(), null, 1);
                if (match3.getCount(checked, 1) < 3) {
                    bombBonusTask.markDone();
                }
            }
            if (timerBonusTask != null) {
                Integer[][] checked = match3.getChain(gems, timerBonusTask.gem.getGemX(), timerBonusTask.gem.getGemY(), null, 1);
                if (match3.getCount(checked, 1) < 3) {
                    timerBonusTask.markDone();
                }
            }
        }

        @Override
        public final void update(float delta) {
            // Auto play
            autoPlay(delta);

            // Balance the chains
            balanceChains(delta);

            // Provide hint if necessary
            provideHint(delta);

            // Create bonus if necessary
            createBonus(delta);

            // Reset chain if user takes too long
            if (!inBonusRound && timeSincePlay > 1)
                chainCount = 0;
        }

        @Override
        public final void paint(float deltas) {
            // Paint gems
            Gem gem;
            for (int i = 0; i < 7; i++) {
                for (int j = 0; j < 8; j++) {
                    gem = gems[i][j];
                    if (gem != null) {
                        gem.paint();
                    }
                }
            }

            // Draw chain count display
            for (int i = 0; i < chainCount; i++) {
                float blue = chainCount > 5 ? 0 : 1;
                game.graphics.drawRect(15 + i * 50, 1130, 35, 90, 1, 1, blue, 1);
            }
        }

        @Override
        public final int getPaintIndex() {
            return 100;
        }
    }

    private final class BonusRound extends Task {

        private final GemsTask gemsTask;
        private float timer;

        public BonusRound(ScreenEx screen, GemsTask gemsTask) {
            super(screen);

            this.gemsTask = gemsTask;
            this.gemsTask.inBonusRound = true;
            this.timer = 5;
        }

        public final void update(float delta) {
            timer -= delta;
            ((ScreenGame) screen).score += 50;
            if (timer <= 0) {
                gemsTask.chainCount = 0;
                gemsTask.inBonusRound = false;
            }
        }

        public final boolean isDone() {
            return timer <= 0;
        }
    }

    private final class ScoreTask extends Task {

        public ScoreTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void paint(float delta) {
            String str = formatter.format(score);
            game.graphics.drawFont(str, fontScore, 590 - fontScore.getWidth(str), 1200);
        }

        @Override
        public final int getPaintIndex() {
            return 300;
        }
    }

    private final class TimerTask extends Task {

        public TimerTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void update(float delta) {
            // Update timer
            timer -= delta;

            // If the timer is 0, end the game
            if (timer < 0) {
                game.data.put("score", score.toString());
                game.setScreenEx(Screens.GAMEOVER);
            }
        }

        @Override
        public final void paint(float delta) {
            Integer time = (int) Math.ceil(timer);
            String str = time.toString();
            game.graphics.drawImage(timerBar, ((60 - time) * -10.283f) + 15, 100);
            game.graphics.drawRect(0, 100, 88, 45, 0.1484375f, 0.1484375f, 0.1484375f, 1);
            game.graphics.drawFont(str, fontTimer, 12, 146);
        }
    }
}
