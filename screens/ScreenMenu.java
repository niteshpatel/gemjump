package com.herodevelop.gemjump.screens;

import com.herodevelop.gemjump.MainStarter.Screens;
import com.herodevelop.gemjump.tasks.artefacts.ProgressTask;
import com.herodevelop.hdfw.GameEx;
import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdfw.tasks.actions.ButtonTask;
import com.herodevelop.hdfw.tasks.actions.SetScreenTask;
import com.herodevelop.hdfw.tasks.transitions.FadeInTask;
import com.herodevelop.hdfw.tasks.transitions.FadeOutTaskFactory;
import com.herodevelop.hdlibgdx.Graphics.Font;
import com.herodevelop.hdlibgdx.Graphics.Image;
import com.herodevelop.hdlibgdx.Utils.Json;

import java.text.NumberFormat;
import java.util.Locale;

public final class ScreenMenu extends ScreenEx {

    private final Image bg;
    private final Image playButton;
    private final Font fontHighscore;
    private final NumberFormat numberFormat;
    private int[] highscores;

    public ScreenMenu(GameEx game) {
        super(game);
        this.bg = new Image("backgrounds/main_menu.png");
        this.playButton = new Image("artefacts/button_play.png");

        // Number formatter
        this.numberFormat = NumberFormat.getInstance(new Locale("en_US"));

        // Setup fonts
        this.fontHighscore = new Font("fonts/terminal-40-black.fnt", false);
    }

    @Override
    protected final void onStart() {
        // Main screen task
        addTask(new BgTask(this));
        addTask(new FadeInTask(this, 400));
        addTask(new ButtonTask(this, playButton, 356, 199,
                new FadeOutTaskFactory(this, 400, new SetScreenTask(this, Screens.PREPARE, 0.1f))));

        // Progress task
        addTask(new ProgressTask(this));

        // Get high scores
        highscores = new Json().fromJson(int[].class, game.data.get("highscores"));
    }

    private final class BgTask extends Task {

        public BgTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void handleInput(float delta) {
            if (game.input.justTouched()) {
                int posX = game.input.getX();
                int posY = game.input.getY();

//                if (posY > 600) {
//                    game.billing.launchPurchaseFlow();
//                }
            }
        }

        @Override
        public final void paint(float delta) {
            game.graphics.drawImage(bg, 0, 0);

            // Draw highscore
            String str = numberFormat.format(highscores[0]);
            game.graphics.drawFont(str, fontHighscore, 150, 453);
        }
    }
}
