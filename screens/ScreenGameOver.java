package com.herodevelop.gemjump.screens;

import com.herodevelop.gemjump.MainStarter.Screens;
import com.herodevelop.gemjump.tasks.artefacts.ProgressTask;
import com.herodevelop.hdfw.GameEx;
import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdfw.tasks.actions.ButtonTask;
import com.herodevelop.hdfw.tasks.actions.SetScreenTask;
import com.herodevelop.hdfw.tasks.transitions.FadeInTask;
import com.herodevelop.hdfw.tasks.transitions.FadeOutTaskFactory;
import com.herodevelop.hdlibgdx.Graphics.Font;
import com.herodevelop.hdlibgdx.Graphics.Image;
import com.herodevelop.hdlibgdx.Utils.Json;

import java.text.NumberFormat;
import java.util.Locale;

public final class ScreenGameOver extends ScreenEx {

    private final Image bg;
    private final Image okayButton;
    private final Font fontScore;
    private final NumberFormat formatter;
    private Integer score;

    public ScreenGameOver(GameEx game) {
        super(game);

        // Background pre-load
        this.bg = new Image("backgrounds/game_over.png");

        // Other images
        this.okayButton = new Image("artefacts/button_okay.png");

        // Setup fonts
        this.fontScore = new Font("fonts/terminal-80.fnt", false);

        // Number formatter
        this.formatter = NumberFormat.getInstance(new Locale("en_US"));
    }

    @Override
    protected final void onStart() {
        // Load score, coins and level progress
        score = Integer.parseInt(game.data.get("score"));
        Integer coins = Integer.parseInt(game.data.get("coins"));
        int level = Integer.parseInt(game.data.get("level"));
        int progress = Integer.parseInt(game.data.get("progress"));

        // Load high scores
        int[] highscores = new Json().fromJson(int[].class, game.data.get("highscores"));

        // Update coins and level process
        coins += (score / 30000) * 5;
        progress += score / 50000;
        if (progress > 100) {
            level += progress / 100;
            progress = progress % 100;
        }

        // Update highscores
        for (int i = 0; i < highscores.length; i++)
            if (score > highscores[i]) {
                // Downshifts the array by one position
                System.arraycopy(highscores, i, highscores, i + 1, highscores.length - 1 - i);
                highscores[i] = score;
                break;
            }

        // Update game data
        game.data.put("level", String.valueOf(level));
        game.data.put("progress", String.valueOf(progress));
        game.data.put("coins", String.valueOf(coins));
        game.data.put("highscores", new Json().toJson(highscores));

        // Update preferences
        game.app.setPreference("level", String.valueOf(level));
        game.app.setPreference("progress", String.valueOf(progress));
        game.app.setPreference("coins", String.valueOf(coins));
        game.app.setPreference("highscores", new Json().toJson(highscores));
        game.app.flushPreferences();

        // Main screen task
        addTask(new BgTask(this));
        addTask(new FadeInTask(this, 400));
        addTask(new ButtonTask(this, okayButton, 360, 200,
                new FadeOutTaskFactory(this, 400, new SetScreenTask(this, Screens.MENU, 0.1f))));

        // Progress task
        addTask(new ProgressTask(this));

        // Score task
        addTask(new ScoreTask(this));
    }

    private final class BgTask extends Task {

        public BgTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void paint(float delta) {
            game.graphics.drawImage(bg, 0, 0);
        }
    }

    private final class ScoreTask extends Task {

        public ScoreTask(ScreenEx screen) {
            super(screen);
        }

        @Override
        public final void paint(float delta) {
            // Draw score
            String str = formatter.format(score);
            game.graphics.drawFont(str, fontScore, 400 - fontScore.getWidth(str) / 2, 800);
        }

        @Override
        public final int getPaintIndex() {
            return 300;
        }
    }
}
