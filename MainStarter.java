package com.herodevelop.gemjump;

import com.herodevelop.gemjump.screens.*;
import com.herodevelop.hdfw.GameEx;
import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdfw.ScreenId;

import java.util.HashMap;

public final class MainStarter extends GameEx {

    @Override
    protected int getVirtualWidth() {
        return 720;
    }

    @Override
    protected int getVirtualHeight() {
        return 1280;
    }

    @Override
    public final void create() {
        super.create();
        screens = new HashMap<ScreenId, ScreenEx>();
        screens.put(Screens.SPLASH, new ScreenSplash(this));
        setScreen(getScreenEx(Screens.SPLASH));
    }

    public final void createEx() {
        screens.put(Screens.MENU, new ScreenMenu(this));
        screens.put(Screens.PREPARE, new ScreenPrepare(this));
        screens.put(Screens.GAME, new ScreenGame(this));
        screens.put(Screens.PAUSE, new ScreenPause(this));
        screens.put(Screens.GAMEOVER, new ScreenGameOver(this));
    }

    public static enum Screens implements ScreenId {
        SPLASH, PREPARE, GAME, PAUSE, MENU, GAMEOVER
    }
}
