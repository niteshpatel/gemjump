package com.herodevelop.gemjump.tasks.artefacts;

import com.herodevelop.hdfw.ScreenEx;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdlibgdx.Graphics.Font;

public final class ProgressTask extends Task {
    private final Font fontLevel;
    private final Font fontProgress;
    private final Font fontCoins;
    private final Integer level;
    private final Integer progress;
    private Integer coins;

    public ProgressTask(ScreenEx screen) {
        super(screen);

        // Setup fonts
        this.fontLevel = new Font("fonts/terminal-40-black.fnt", false);
        this.fontProgress = new Font("fonts/terminal-40-black.fnt", false);
        this.fontCoins = new Font("fonts/terminal-40-black.fnt", false);

        // Set level and progress
        this.level = Integer.parseInt(game.data.get("level"));
        this.progress = Integer.parseInt(game.data.get("progress"));
        this.coins = Integer.parseInt(game.data.get("coins"));
    }

    public void setCoins(Integer coins) {
        this.coins = coins;
    }

    public final void paint(float delta) {
        // Draw level
        String str = Integer.toString(level);
        game.graphics.drawFont(str, fontLevel, 584, 1233);

        // Draw progress
        str = Integer.toString(progress) + "%";
        game.graphics.drawFont(str, fontProgress, 584, 1185);

        // Draw coins
        str = Integer.toString(coins);
        game.graphics.drawFont(str, fontCoins, 584, 1137);
    }

    @Override
    public final int getPaintIndex() {
        return 300;
    }
}