package com.herodevelop.gemjump.tasks.effects;

import com.herodevelop.gemjump.Gem;
import com.herodevelop.gemjump.screens.ScreenGame;
import com.herodevelop.hdfw.tasks.Task;

public final class BlockedGemTask extends Task {

    private final ScreenGame screen;
    private final Gem gem;
    private double timer;

    public BlockedGemTask(ScreenGame screen, Gem gem) {
        super(screen);

        this.screen = screen;
        this.gem = gem;

        // Set blocked gem image
        gem.setImage(screen.getGemImagesBlocked().get(gem.gemId));
        gem.setBlockedState(true);

        // Initialise timer
        this.timer = 0.7;
    }

    @Override
    public final void update(float delta) {
        timer -= delta;
        if (timer <= 0) {
            gem.setImage(screen.getGemImagesNormal().get(gem.gemId));
            gem.setBlockedState(false);
        }
    }

    @Override
    public final boolean isDone() {
        return timer <= 0;
    }
}
