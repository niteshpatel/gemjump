package com.herodevelop.gemjump.tasks.bonuses;

import com.herodevelop.gemjump.Gem;
import com.herodevelop.gemjump.screens.ScreenGame;
import com.herodevelop.gemjump.screens.ScreenGame.GemsTask;
import com.herodevelop.hdlibgdx.Audio.Sound;
import com.herodevelop.hdlibgdx.Graphics.Image;

public class TimerBonusTask extends BonusTask {

    public TimerBonusTask(ScreenGame screen, GemsTask gemsTask, Gem gem) {
        super(screen, gemsTask, gem);

        // Setup sound and image
        this.sound = new Sound("sounds/gem_bomb.ogg");
        this.icon = new Image("bonuses/timer.png");
    }

    @Override
    public void doBonus(Integer[][] chain) {
        // Increase time
        screen.timer += 5;
        if (screen.timer > 60)
            screen.timer = 60f;

        // Now remove the games
        int count = screen.match3.getCount(chain, 1);
        screen.match3.removeChain(screen.gems, chain, 1);
        screen.match3.dropDown(screen.gems);

        // Post drop tasks
        gemsTask.postDrop(count);
        sound.play(1);
    }

    @Override
    public final void destroy() {
        gemsTask.timerBonusTask = null;
    }
}
