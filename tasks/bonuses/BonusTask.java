package com.herodevelop.gemjump.tasks.bonuses;

import com.herodevelop.gemjump.Gem;
import com.herodevelop.gemjump.screens.ScreenGame;
import com.herodevelop.gemjump.screens.ScreenGame.GemsTask;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdlibgdx.Audio.Sound;
import com.herodevelop.hdlibgdx.Graphics.Image;

public abstract class BonusTask extends Task {

    public final Gem gem;
    protected final ScreenGame screen;
    protected final GemsTask gemsTask;
    protected Image icon;
    protected Sound sound;
    protected double timer;
    protected boolean done;

    public BonusTask(ScreenGame screen, GemsTask gemsTask, Gem gem) {
        super(screen);

        this.screen = screen;
        this.gemsTask = gemsTask;
        this.gem = gem;

        // Initialise timer and done boolean
        this.timer = 5;
        this.done = false;
    }

    @Override
    public void handleInput(float delta) {
        if (game.input.justTouched()) {
            int posX = game.input.getX();
            int posY = game.input.getY();

            // Check for gem presses
            if (posX >= 5 && posY >= 110 && posX < 355 && posY < 510) {

                // Get the clicked gem positions
                int clickedGemX = (int) Math.floor((posX - 5) / 50);
                int clickedGemY = (int) Math.floor((posY - 110) / 50);

                // Get our bonus gem position
                int gemX = gem.getGemX();
                int gemY = gem.getGemY();

                // Check all the gems in the chain to see if any of them match our bonus gem
                boolean found = false;
                Integer[][] chain = screen.match3.getChain(screen.gems, clickedGemX, clickedGemY, null, 1);
                for (int i = 0; i < 7; i++) {
                    for (int j = 0; j < 8; j++) {
                        if (chain[i][j] == 1 && i == gemX && j == gemY) {
                            found = true;
                            break;
                        }
                    }
                }

                // If our bonus gem is in the chain of clicked gems
                if (found) {
                    gemsTask.setSkippingInput(true);

                    // Mark task as done
                    done = true;

                    // Bonus specific tasks
                    doBonus(chain);
                }
            }
        }
    }

    public abstract void doBonus(Integer[][] chain);

    @Override
    public final void update(float delta) {
        timer -= delta;
    }

    @Override
    public final void paint(float delta) {
        game.graphics.drawImage(icon, gem.getPosX(), gem.getPosY());
    }

    @Override
    public boolean isDone() {
        return timer <= 0 || done;
    }

    public void markDone() {
        done = true;
    }

    @Override
    public abstract void destroy();

    @Override
    public final int getPaintIndex() {
        return 300;
    }

    @Override
    public final int getUpdateIndex() {
        // Ensure we run before the GemsTask
        return -100;
    }
}
