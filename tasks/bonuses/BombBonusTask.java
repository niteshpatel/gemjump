package com.herodevelop.gemjump.tasks.bonuses;

import com.herodevelop.gemjump.Gem;
import com.herodevelop.gemjump.screens.ScreenGame;
import com.herodevelop.gemjump.screens.ScreenGame.GemsTask;
import com.herodevelop.hdlibgdx.Audio.Sound;
import com.herodevelop.hdlibgdx.Graphics.Image;

public class BombBonusTask extends BonusTask {

    public BombBonusTask(ScreenGame screen, GemsTask gemsTask, Gem gem) {
        super(screen, gemsTask, gem);

        // Setup sound and image
        this.sound = new Sound("sounds/gem_bomb.ogg");
        this.icon = new Image("bonuses/bomb.png");
    }

    public void extendChain(Integer[][] chain) {
        // Extend chain by 2 on all sides
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 8; j++) {
                if (chain[i][j] == 1) {
                    // Row 1
                    if (i > 0 && j > 1)
                        chain[i - 1][j - 2] = 2;
                    if (j > 1)
                        chain[i][j - 2] = 2;
                    if (i < 6 && j > 1)
                        chain[i + 1][j - 2] = 2;
                    // Row 2
                    if (i > 1 && j > 0)
                        chain[i - 2][j - 1] = 2;
                    if (i > 0 && j > 0)
                        chain[i - 1][j - 1] = 2;
                    if (j > 0)
                        chain[i][j - 1] = 2;
                    if (i < 6 && j > 0)
                        chain[i + 1][j - 1] = 2;
                    if (i < 5 && j > 0)
                        chain[i + 2][j - 1] = 2;
                    // Row 3
                    if (i > 1)
                        chain[i - 2][j] = 2;
                    if (i > 0)
                        chain[i - 1][j] = 2;
                    chain[i][j] = 2;
                    if (i < 6)
                        chain[i + 1][j] = 2;
                    if (i < 5)
                        chain[i + 2][j] = 2;
                    // Row 4
                    if (i > 1 && j < 7)
                        chain[i - 2][j + 1] = 2;
                    if (i > 0 && j < 7)
                        chain[i - 1][j + 1] = 2;
                    if (j < 7)
                        chain[i][j + 1] = 2;
                    if (i < 6 && j < 7)
                        chain[i + 1][j + 1] = 2;
                    if (i < 5 && j < 7)
                        chain[i + 2][j + 1] = 2;
                    // Row 5
                    if (i > 0 && j < 6)
                        chain[i - 1][j + 2] = 2;
                    if (j < 6)
                        chain[i][j + 2] = 2;
                    if (i < 6 && j < 6)
                        chain[i + 1][j + 2] = 2;
                }
            }
        }
        // Convert 2's to 1's
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 8; j++) {
                if (chain[i][j] == 2) {
                    chain[i][j] = 1;
                }
            }
        }
    }

    @Override
    public final void doBonus(Integer[][] chain) {
        // Extend the chain on all sides
        extendChain(chain);

        // Now remove the gems
        int count = screen.match3.getCount(chain, 1);
        screen.match3.removeChain(screen.gems, chain, 1);
        screen.match3.dropDown(screen.gems);

        // Post drop tasks
        gemsTask.postDrop(count);
        sound.play(1);
    }

    @Override
    public final void destroy() {
        gemsTask.bombBonusTask = null;
    }
}
