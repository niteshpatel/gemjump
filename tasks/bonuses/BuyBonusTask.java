
package com.herodevelop.gemjump.tasks.bonuses;

import com.herodevelop.gemjump.Constants.BonusType;
import com.herodevelop.gemjump.screens.ScreenPrepare;
import com.herodevelop.hdfw.tasks.ReuseTask;

public class BuyBonusTask extends ReuseTask {

    private final ScreenPrepare screen;
    private final BonusType bonusType;
    private final int cost;
    private final float initialDelay;
    private float delay;
    private boolean done;

    public BuyBonusTask(ScreenPrepare screen, BonusType bonusType, int cost) {
        super(screen);

        this.screen = screen;
        this.bonusType = bonusType;
        this.cost = cost;
        this.initialDelay = 0.2f;
        reset();
    }

    @Override
    public void reset() {
        delay = initialDelay;
        done = false;
    }

    @Override
    public void update(float delta) {
        delay -= delta;
        if (delay <= 0) {
            int coins = Integer.parseInt(game.data.get("coins"));
            int bonusCount = Integer.parseInt(game.data.get(bonusType.name()));

            // Exit if we do not have enough coins
            if (coins - cost < 0) {
                done = true;
                return;
            }

            // Exit if we already have 9 items)
            if (bonusCount >= 9) {
                done = true;
                return;
            }

            // Reduce the coins by cost
            coins -= cost;
            screen.progressTask.setCoins(coins);
            game.data.put("coins", String.valueOf(coins));

            // Increase the number of bonus items by one
            bonusCount += 1;
            screen.updateBonusCount(bonusType);
            game.data.put(bonusType.name(), String.valueOf(bonusCount));

            // Update preferences
            game.app.setPreference("coins", String.valueOf(coins));
            game.app.setPreference(bonusType.name(), String.valueOf(bonusCount));
            game.app.flushPreferences();
            done = true;
        }
    }

    @Override
    public final boolean isDone() {
        return done;
    }
}
