package com.herodevelop.gemjump.tasks.bonuses;

import com.herodevelop.gemjump.Constants.BonusType;
import com.herodevelop.gemjump.screens.ScreenPrepare;
import com.herodevelop.hdfw.tasks.Task;
import com.herodevelop.hdfw.tasks.TaskFactory;

public final class BuyBonusTaskFactory extends TaskFactory {

    private final ScreenPrepare screen;
    private final BonusType bonusType;
    private final int cost;

    public BuyBonusTaskFactory(ScreenPrepare screen, BonusType bonusType, int cost) {
        this.screen = screen;
        this.bonusType = bonusType;
        this.cost = cost;
    }

    @Override
    public final Task newTask() {
        return new BuyBonusTask(screen, bonusType, cost);
    }
}
